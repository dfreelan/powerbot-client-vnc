#!/bin/bash
export DISPLAY=:0
export TIGERVNC_VER=tigervnc-1.8.0.x86_64

function client_download {
    echo "Downloading powerbot"
	echo "basic update"    
    STATUSCODEPOWERBOT=$(wget -N -L 'https://www.powerbot.org/download/?jar')
    echo "END STATUS CODE "
    echo $STATUSCODEPOWERBOT
    FILESIZE=$(stat -c%s "/root/index.html?jar")
    echo "FILE SIZE END"
    echo $FILESIZE
    if [ "$FILESIZE" -lt 1001 ]
    then
        sleep 900
        client_download
    fi
}

function jagexclient_download {
    echo "Downloading jagex appletviewer"
    STATUSCODE_RS=$(wget -N -L 'http://www.runescape.com/downloads/jagexappletviewer.jar')
    
    FILESIZE=$(stat -c%s "/root/jagexappletviewer.jar")
    # is around 60kb in size
    if [ "$FILESIZE" -lt 50000 ]
    then
        sleep 900
        jagexclient_download
    fi
}

function check_updates {
    cd /root
    client_download
    jagexclient_download
}

function setup_vnc_password {
    # Configure tigervnc
    echo "configuring tigervnc..."
    rm -rf /root/.vnc && mkdir /root/.vnc && printf "$VNC_PASSWORD\n$VNC_PASSWORD\n\n" | /root/$TIGERVNC_VER/usr/bin/vncpasswd \
    && echo -e '#!/bin/sh\nunset SESSION_MANAGER\nunset DBUS_SESSION_BUS_ADDRESS\ncompton &' > /root/.vnc/xstartup \
    && chmod u+x /root/.vnc/xstartup
}

function check_running {
    countprocesses=$(pgrep -f runescape.com | wc -l)
    if [ $countprocesses -lt 1 ]
    then
        start_client
    fi
}

function start_client {
echo "startin client updataed YOOOOOOOOOOOOOOOOO"
chmod 755 /root/jagexappletviewer.jar    
#java -Dcom.jagex.config=http://oldschool.runescape.com/l=en/jav_config.ws -Xmx1450M -classpath /root/index.html?jar -Dfile.encoding=UTF-8 -jar /root/jagexappletviewer.jar	

java -Duser.home=$HOME -Djava.class.path=/root/index.html?jar -Dcom.jagex.config=http://oldschool.runescape.com/jav_config.ws jagexappletviewer &


echo "end the java run"

}

function initialise_environment {
echo "init environment,source"    
source /root/.profile
echo "env"
    env
echo "rm tmp"
    rm -rf /tmp/.*
echo "setup pass"
    if [ ! -z ${VNC_PASSWORD+x} ]
    then
        setup_vnc_password
    fi
echo "starting vnc server"
    /root/$TIGERVNC_VER/usr/bin/vncserver :0 &
echo "sleep for a moment"
    sleep 5
echo "xcomp"
    xcompmgr &
echo "fluxbox"
    startx startfluxbox &
echo "go to root"
    cd /root/
echo "check for updateS"
    check_updates
echo "start the client"
    start_client
    start_client
}

function main {
    initialise_environment
    check_updates
    sleep 900
    
    # loop every 15 minutes
    for i in {1..100000}
    do
        check_updates
        check_running
        sleep 900
    done
}
main
shutdown -h 0
